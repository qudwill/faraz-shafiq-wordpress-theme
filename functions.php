<?php
  define('GOOGLE_API_KEY', 'AIzaSyCZ8tq6k5WmPOemlEa4tMD361MtaNI1pyE');

  $template_directory = get_template_directory();


  /**
   ** Include styles & scripts
   */

  require_once $template_directory . '/inc/assets.php';


  /**
   ** Registering navigation menu
   */
  require_once $template_directory . '/inc/nav.php';

  
  /**
   ** Options page
   */
  require_once $template_directory . '/inc/options.php';


  /**
   ** Google Maps API Key for Advanced Custom Fields Pro
   */

  add_filter('acf/settings/google_api_key', function() {
    return GOOGLE_API_KEY;
  });


  /**
   ** Add Title Tag
   */

  add_theme_support('title-tag');


  /**
   ** Enable post thumbnails
   */

  add_theme_support('post-thumbnails');


  /**
   ** Favicon Settings
   */

  require_once $template_directory . '/inc/theme-favicon-settings.php';


  /**
   ** Include Custom VC Elements
   */

  require_once $template_directory . '/inc/custom-vc-elements.php';


  /**
   ** Widgets
   */

  require_once $template_directory . '/inc/widgets.php';


  /**
   ** Remove WP Emoji
   */

  remove_action('wp_head', 'print_emoji_detection_script', 7);
  remove_action('wp_print_styles', 'print_emoji_styles');
  remove_action('admin_print_scripts', 'print_emoji_detection_script');
  remove_action('admin_print_styles', 'print_emoji_styles');


  /**
   ** Custom Shortcodes
   */

  require_once $template_directory . '/inc/shortcodes.php';
?>