'use strict';

/**
 ** jQuery noConflict Mode On
 */

var $ = jQuery.noConflict();


/**
 ** VC Client Carousel
 */

var VCClientCarousel = (function() {
	function _initCarousel() {
		$('#vc__client-carousel').slick({
			infinite: true,
			slidesToShow: 6,
			slidesToScroll: 1,
			autoplay: true,
			autoplaySpeed: 2000,
			responsive: [
				{
					breakpoint: 991,
					settings: {
						slidesToShow: 4
					}
				},
				{
					breakpoint: 767,
					settings: {
						slidesToShow: 3
					}
				},
				{
					breakpoint: 480,
					settings: {
						slidesToShow: 2
					}
				}
			]
		});
	}

	return {
		init: function() {
			_initCarousel();
		}
	}
}());


/**
 ** Background Icon Generation
 */

var BackgroundIconsGeneration = (function() {
	var icons = ['google-plus', 'contract', 'eye', 'thumbs-up', 'pointer', 'pound-sterling', 'dollar-symbol', 'bing', 'cell-phone', 'social', 'social-1', 'linkedin-logo', 'youtube-logo', 'twitter-logo-silhouette', 'facebook-logo'];
	var tempIcons = icons;

	function _generate() {
		var $iconsLeft2 = $('.icons-left-2');
		var $iconsRight3 = $('.icons-right-3');
		var $iconsLeft4 = $('.icons-left-4');
		var $iconsRight2 = $('.icons-right-2');

		$iconsLeft2.each(function() {
			_generateIcon($(this), 'icons-left-2', 2);
		});

		$iconsRight3.each(function() {
			_generateIcon($(this), 'icons-right-3', 3);
		});

		$iconsLeft4.each(function() {
			_generateIcon($(this), 'icons-left-4', 4);
		});
	}

	function _checkForIcons() {
		(tempIcons.length == 0) ? tempIcons = icons : '';
	}

	function _generateIcon($this, className, count) {
		_checkForIcons();

		var html = '';

		for (var i = 0; i < count; i++) {
			var randIndex = Math.floor(Math.random() * tempIcons.length);
			var icon = tempIcons[randIndex];
	
			tempIcons.splice(randIndex, 1);
	
			html = '<div class="background-icon '+ className + '-' + i + '" style="background-image: url(' + TEMPLATE_DIRECTORY_URI + '/dist/svg/' + icon + '.svg);"></div>';
	
			$this.prepend(html);
		}
	}

	return {
		init: function() {
			_generate();
		}
	}
}());


/**
 ** Navigation
 */

var Nav = (function() {
	var $nav = $('#header__nav-inner');
	var $navButton = $('#header__nav-button');

	function setupListeners() {
		$navButton.on('click', _toggleNav);
	}

	function _toggleNav(e) {
		e.preventDefault();
		$nav.toggleClass('header__nav-inner--opened');
		$navButton.toggleClass('header__nav-button--opened');
	}

	return {
		init: function() {
			setupListeners();
		}
	}
}());


/**
 ** Skrollr
 */

var Skrollr = (function() {
	function _initSkrollr() {
		var s = skrollr.init();

  	if (s.isMobile()) {
		  s.destroy();
		}
	}

	return {
		init: function() {
			_initSkrollr();
		}
	}
}());


/**
 ** VC Images Slider
 */

var VCImagesSlider = (function() {
	function _initCarousel() {
		$('#vc__images-slider').slick({
			infinite: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			autoplay: true,
			autoplaySpeed: 2000
		});
	}

	return {
		init: function() {
			_initCarousel();
		}
	}
}());


/**
 ** WPCF7
 */

var WPCF7 = (function() {
  function setupListeners() {
    $('.wpcf7 input').on('input', _validate);
    $('.wpcf7 .cf7__input--select').on('change', _validateTextarea);
    $('.wpcf7 input[type="checkbox"]').on('change', _validateCheckbox);
    $('.wpcf7 textarea').on('input', _validateTextarea);
    $('.wpcf7').on('wpcf7:mailsent', _showThankYou);
    $('.wpcf7').on('wpcf7:submit', _submit);
    $('#cf7__submit').on('click', _loading);
  }

  function _showThankYou() {
    $('#cf7').hide();
    $('#cf7__thanks').fadeIn();
  }

  function _validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    
    return re.test(email);
  }

  function _invalid(elem) {
    elem.removeClass('wpcf7-not-valid').addClass('wpcf7-valid');
  }

  function _valid(elem) {
    elem.removeClass('wpcf7-valid').addClass('wpcf7-not-valid');
  }

  function _validate() {
    var $this = $(this);

    var type = $this.attr('type');
    var val = $this.val();

    if (type == 'text') {
      if (val != '') {
        _invalid($this);
      } else {
        _valid($this);
      }
    }

    if (type == 'email') {
      if ( _validateEmail(val) ) {
        _invalid($this);
      } else {
        _valid($this);
      }
    }

    if (type == 'tel') {
      if ( /^\d+$/.test(val) ) {
        _invalid($this);
      } else {
        _valid($this);
      }
    }
  }

  function _validateTextarea() {
    var $this = $(this);

    if ($this.val() != '') {
      _invalid($this);
    } else {
      _valid($this);
    }
  }

  function _validateCheckbox() {
  	var $this = $(this);

  	if ($this.is(':checked')) {
  		$this.parent().parent().removeClass('wpcf7-not-valid');
  	}
  }

  function _loading() {
    var $this = $(this);

    $this.html('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
  }

  function _submit() {
    var $this = $(this);
    var submitButton = $this.find('#cf7__submit');

    if ($('.wpcf7-form').hasClass('invalid')) {
      submitButton.html('Get Started');
    }
  }

  return {
    init: function() {
      setupListeners();
    }
  }
}());


/**
 ** Back To The Top Button
 */

var BackToTheTop = (function() {
	var topShow = 500;
	var delay = 1000;
	var button = $('#go-back-button');

	function setupListeners() {
		button.on('click', _scrollToTop);
		$(window).on('scroll', _toggleButton);
	}

	function _scrollToTop(e) {
		e.preventDefault();

		$('body, html').animate({
      scrollTop: 0
    }, delay);
	}

	function _toggleButton() {
		($(this).scrollTop() > topShow) ? button.stop().fadeIn(100) : button.stop().fadeOut(100);
	}

	return {
		init: function() {
			setupListeners();
		}
	}
}());


/**
 ** Module Initialization
 */

$(document).ready(function() {
	Skrollr.init();
	VCClientCarousel.init();
	BackgroundIconsGeneration.init();
	Nav.init();
	VCImagesSlider.init();
	WPCF7.init();
	BackToTheTop.init();
});