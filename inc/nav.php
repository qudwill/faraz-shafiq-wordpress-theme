<?php
  /**
   ** Registering navigation menu
   */

  add_action('after_setup_theme', function() {
    register_nav_menus(
      array(
        'primary' => 'Main Navigation',
        'footer' => 'Footer Navigation',
      )
    );
  });
?>