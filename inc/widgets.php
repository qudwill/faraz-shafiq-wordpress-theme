<?php
  /**
   ** Footer widgets initialization
   */

  register_sidebar(
  	array(
      'name' => __('Widget Area 1'),
      'id' => 'widget_area_1',
      'before_widget' => '<li class="footer__widgets-item">',
      'after_widget' => '</li>',
      'before_title' => '<h5 class="footer__widgets-title">',
      'after_title' => '</h5>',
  	)
  );

  register_sidebar(
    array(
      'name' => __('Widget Area 2'),
      'id' => 'widget_area_2',
      'before_widget' => '<li class="footer__widgets-item">',
      'after_widget' => '</li>',
      'before_title' => '<h5 class="footer__widgets-title">',
      'after_title' => '</h5>',
    )
  );

  register_sidebar(
    array(
      'name' => __('Widget Area 3'),
      'id' => 'widget_area_3',
      'before_widget' => '<li class="footer__widgets-item">',
      'after_widget' => '</li>',
      'before_title' => '<h5 class="footer__widgets-title">',
      'after_title' => '</h5>',
    )
  );

  register_sidebar(
    array(
      'name' => __('Bottom Widget Area'),
      'id' => 'widget_area_bottom',
      'before_widget' => '<div class="bottom-widget">',
      'after_widget' => '</div>',
      'before_title' => '<h5 class="footer__widgets-title">',
      'after_title' => '</h5>',
    )
  );
?>