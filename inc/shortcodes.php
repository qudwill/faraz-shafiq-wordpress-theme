<?php
	/**
	 ** Latest Tweet Shortcode
	 */

	function latest_tweet() {
		require_once(get_template_directory() . '/vendor/twitter-api-php/TwitterAPIExchange.php');

		$settings = array(
  		'consumer_key' => 'utoHZY3zFcTC7VD5WJuMxGLZX',
  		'consumer_secret' => 'azUmgbOvaiKksvr0eCj1sREFymYUZ5rOBnDEG81pFCYrtEVybk',
  		'oauth_access_token' => '589912030-H5fAMnkbuQYJMjvuRIFbFR48S1oQnmBcwPZBsikJ',
  		'oauth_access_token_secret' => 'rDoNiJbqhPrHSeOPvEMuHxoY4y96RIrAfbqY1AYXFbufR',
		);

		$screen_name = 'farazshafiq';
		$url = 'https://api.twitter.com/1.1/statuses/user_timeline.json';
		$getfield = "?screen_name={$screen_name}&exclude_replies=true&count=1";
		$requestMethod = 'GET';
		$twitter = new TwitterAPIExchange($settings);
		$user_timeline = $twitter->setGetfield($getfield)->buildOauth($url, $requestMethod)->performRequest();
		$user_timeline = json_decode($user_timeline);
		$date = new DateTime($user_timeline[0]->created_at);
		$html = '<div class="latest-tweet">';
		
		$html .= '<div class="latest-tweet__content">' . $user_timeline[0]->text . '</div>';
		$html .= '<div class="latest-tweet__author group">';
		$html .= '<div class="latest-tweet__author-avatar">';
		$html .= '<a href="https://twitter.com/' . $user_timeline[0]->user->screen_name . '" class="latest-tweet__image-link">';
		$html .= '<img src="' . $user_timeline[0]->user->profile_image_url_https . '" alt="' . $user_timeline[0]->user->name . '" class="latest-tweet__author-image">';
		$html .= '</a>';
		$html .= '</div>';
		$html .= '<div class="latest-tweet__author-info">';
		$html .= '<a href="https://twitter.com/' . $user_timeline[0]->user->screen_name . '" class="latest-tweet__author-name">@' . $user_timeline[0]->user->screen_name . '</a>';
		$html .= '<div class="latest-tweet__author-date">' . $date->format('d M Y') . '</div>';
		$html .= '</div>';
		$html .= '</div>';
		$html .= '</div>';

		return $html;
	}

	add_shortcode('latest_tweet', 'latest_tweet');
?>