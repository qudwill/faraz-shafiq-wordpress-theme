<?php
  /**
   ** Options page
   */

  if ( function_exists('acf_add_options_page') ) {
    acf_add_options_page(
      array(
        'page_title' => 'Theme General Settings',
        'menu_title' => 'Theme Settings',
        'menu_slug' => 'theme-general-settings',
        'capability' => 'edit_posts',
        'redirect' => false,
      )
    );
  
    acf_add_options_sub_page(
      array(
        'page_title' => 'Theme Favicon Settings',
        'menu_title' => 'Favicon',
        'parent_slug' => 'theme-general-settings',
      )
    );
  
    acf_add_options_sub_page(
      array(
        'page_title' => 'Theme Social Icons Settings',
        'menu_title' => 'Social Networks',
        'parent_slug' => 'theme-general-settings',
      )
    );
  
    acf_add_options_sub_page(
      array(
        'page_title' => 'Professional Standards Settings',
        'menu_title' => 'Professional Standards',
        'parent_slug' => 'theme-general-settings',
      )
    );
  
    acf_add_options_sub_page(
      array(
        'page_title' => 'CTA Settings',
        'menu_title' => 'CTA',
        'parent_slug' => 'theme-general-settings',
      )
    );
  
    acf_add_options_sub_page(
      array(
        'page_title' => 'Get In Touch Settings',
        'menu_title' => 'Get In Touch',
        'parent_slug' => 'theme-general-settings',
      )
    );
  }
?>