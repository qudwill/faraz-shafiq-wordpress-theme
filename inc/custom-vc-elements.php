<?php
  /**
   ** Include Custom VC Elements
   */

  global $template_directory;

  require_once $template_directory . '/vc-elements/vc-planet.php';
  require_once $template_directory . '/vc-elements/vc-client-carousel.php';
  require_once $template_directory . '/vc-elements/vc-project.php';
  require_once $template_directory . '/vc-elements/vc-latest-posts.php';
  require_once $template_directory . '/vc-elements/vc-blog.php';
  require_once $template_directory . '/vc-elements/vc-quote.php';
  require_once $template_directory . '/vc-elements/vc-case-meta.php';
  require_once $template_directory . '/vc-elements/vc-images-slider.php';
  require_once $template_directory . '/vc-elements/vc-google-map.php';
  require_once $template_directory . '/vc-elements/vc-social-networks.php';
?>