<?php
	/**
   ** Include styles & scripts
   */

  add_action('wp_enqueue_scripts', 'smd_scripts');

  function smd_scripts() {
    $template_directory_uri = get_template_directory_uri();

    /**
     ** CSS
     */

    wp_enqueue_style('pt-sans-font', '//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i');
    wp_enqueue_style('main', $template_directory_uri . '/dist/css/main.css');


    /**
     ** Dev JS
     */

    // wp_enqueue_script('skrollr', $template_directory_uri . '/node_modules/skrollr/dist/skrollr.min.js', array('jquery'), false, true);
    // wp_enqueue_script('slick-carousel', $template_directory_uri . '/node_modules/slick-carousel/slick/slick.min.js', array('jquery'), false, true);
    // wp_register_script('google-maps-js-api', '//maps.googleapis.com/maps/api/js?key=' . GOOGLE_API_KEY . '&callback=initMap', array(), false, true);
    // wp_enqueue_script('app', $template_directory_uri . '/src/js/app.js', array('jquery'), false, true);


    /**
     ** Build JS
     */

    wp_register_script('google-maps-js-api', '//maps.googleapis.com/maps/api/js?key=' . GOOGLE_API_KEY . '&callback=initMap', array(), false, true);
    wp_enqueue_script('app', $template_directory_uri . '/dist/js/app.js', array('jquery'), false, true);
  }
?>