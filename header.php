<?php
	$common_faviconico = get_field('common_faviconico', 'option');
	$favicon_57x57 = get_field('favicon_57x57', 'option');
	$favicon_72 = get_field('favicon_72', 'option');
	$favicon_114x114 = get_field('favicon_114x114', 'option');
	$favicon_120x120 = get_field('favicon_120x120', 'option');
	$favicon_144 = get_field('favicon_144', 'option');
	$favicon_152 = get_field('favicon_152', 'option');
?>
<!doctype html>
<html <?php language_attributes(); ?>>
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta charset="<?php bloginfo('charset'); ?>">
		<?php if ($common_faviconico) : ?>
			<link rel="shortcut icon" sizes="16x16 24x24 32x32 48x48 64x64" href="<?php echo $common_faviconico; ?>">
		<?php endif; ?>
		<?php if ($favicon_57x57) : ?>
			<link rel="apple-touch-icon" sizes="57x57" href="<?php echo $favicon_57x57; ?>">
			<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo $favicon_57x57; ?>">
		<?php endif; ?>
		<?php if ($favicon_72) : ?>
			<link rel="apple-touch-icon" sizes="72x72" href="<?php echo $favicon_72; ?>">
		<?php endif; ?>
		<?php if ($favicon_114x114) : ?>
			<link rel="apple-touch-icon" sizes="114x114" href="<?php echo $favicon_114x114; ?>">
		<?php endif; ?>
		<?php if ($favicon_120x120) : ?>
			<link rel="apple-touch-icon" sizes="120x120" href="<?php echo $favicon_120x120; ?>">
		<?php endif; ?>
		<?php if ($favicon_144) : ?>
			<link rel="apple-touch-icon" sizes="144x144" href="<?php echo $favicon_144; ?>">
			<meta name="msapplication-TileImage" content="<?php echo $favicon_144; ?>">
		<?php endif; ?>
		<?php if ($favicon_152) : ?>
		  <link rel="apple-touch-icon" sizes="152x152" href="<?php echo $favicon_152; ?>">
		<?php endif; ?>
		<meta name="application-name" content="<?php bloginfo(); ?>">
		<!--[if lt IE 9]>
			<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<?php
			get_template_part('template-parts/header', 'variables');
			wp_head();
		?>
	</head>
	<body>
		<div class="wrapper">
			<header class="header">
				<div class="header__top group">
					<div class="container">
						<?php
							get_template_part('template-parts/header', 'socials');
							get_template_part('template-parts/header', 'contacts');
						?>
					</div>
				</div>
				<div class="header__nav group">
					<div class="container">
						<?php
							get_template_part('template-parts/header', 'logo');
							get_template_part('template-parts/header', 'nav');
						?>
					</div>
				</div>
			</header>
			<div class="content">