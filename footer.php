		</div>
		<?php
			if ( !is_front_page() && !is_page(21) && !is_page(23) ) {
				get_template_part('template-parts/get-in-touch');
			}
		?>
		<?php get_template_part('template-parts/cta'); ?>
		<footer class="footer">
			<div class="container">
				<?php
					get_template_part('template-parts/footer', 'widgets');
					get_template_part('template-parts/footer', 'professional-standards');
				?>
				<div class="footer__bottom group">
					<?php
						get_template_part('template-parts/footer', 'nav');
						get_template_part('template-parts/footer', 'copyright');
					?>
				</div>
			</div>
		</footer>
		<?php
			get_template_part('template-parts/go-back-button');
			wp_footer();
		?>
	</body>
</html>