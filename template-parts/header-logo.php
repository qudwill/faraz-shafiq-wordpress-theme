<?php $logo = get_field('logo', 'option'); ?>
<?php if ($logo) : ?>
	<div class="header__nav-logo">
		<a href="<?php echo esc_url( home_url('/') ); ?>" class="header__logo-link">
			<img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>" class="header__logo-image">
		</a>
	</div>
<?php endif; ?>