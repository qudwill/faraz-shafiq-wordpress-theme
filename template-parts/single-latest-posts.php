<div class="single__latest-posts">
	<div class="container">
		<?php echo do_shortcode('[vc_row full_width="stretch_row"][vc_column][vc_custom_heading text="Latest Posts" font_container="tag:h2|font_size:26px|text_align:center" use_theme_fonts="yes"][vc_latest_posts][vc_empty_space height="40px"][vc_btn title="See All Posts" style="outline-custom" outline_custom_color="#323c41" outline_custom_hover_background="#323c41" outline_custom_hover_text="#ffffff" shape="round" align="center" link="url:http%3A%2F%2Ffarazshafiq.loc%2Fblog%2F|title:See%20All%20Posts||"][vc_empty_space height="25px"][/vc_column][/vc_row]'); ?>
	</div>
</div>