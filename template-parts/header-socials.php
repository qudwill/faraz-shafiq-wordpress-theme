<?php $social_networks = get_field('social_networks', 'option'); ?>
<?php if ( !empty($social_networks) ) : ?>
	<div class="header__top-socials">
		<ul class="header__socials-list group">
			<?php foreach ($social_networks as $social_network) : ?>
				<li class="header__socials-item">
					<a href="<?php echo $social_networks['link']; ?>" class="header__socials-link" target="_blank" rel="nofollow">
						<img src="<?php echo $social_network['icon']['url']; ?>" alt="<?php echo $social_network['icon']['alt']; ?>" class="header__socials-icon">
					</a>
				</li>
			<?php endforeach; ?>
		</ul>
	</div>
<?php endif; ?>