<?php
	$email = get_field('email', 'option');
	$phone_number = get_field('phone_number', 'option');
?>
<?php if ($email || $phone_number) : ?>
	<div class="header__top-contacts">
		<ul class="header__contacts-list group">
			<?php if ($email) : ?>
				<li class="header__contacts-item">
					<a href="mailto:<?php echo $email; ?>" class="header__contacts-link"><?php echo $email; ?></a>
				</li>
			<?php endif; ?>
			<?php if ($phone_number) : ?>
				<li class="header__contacts-item">
					<a href="tel:<?php echo $phone_number; ?>" class="header__contacts-link"><?php echo $phone_number; ?></a>
				</li>
			<?php endif; ?>
		</ul>
	</div>
<?php endif; ?>