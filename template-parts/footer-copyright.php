<?php $copyright = get_field('copyright', 'option'); ?>
<?php if ($copyright) : ?>
	<div class="footer__bottom-copyright"><?php echo $copyright; ?></div>
<?php endif; ?>