<?php
	$template_directory_URI = get_template_directory_uri();
	$permalink = get_permalink();

	global $title;
?>
<div class="single-content__share">
	<div class="single__share-label"><?php _e('Share', 'smd'); ?></div>
	<ul class="single__share-list">
		<li class="single__share-item">
			<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $permalink; ?>" class="single__share-link" target="_blank" rel="nofollow">
				<img src="<?php echo $template_directory_URI; ?>/dist/images/facebook.png" alt="<?php _e('Facebook', 'smd'); ?>" class="single__share-icon">
			</a>
		</li>
		<li class="single__share-item">
			<a href="https://twitter.com/home?status=<?php echo $permalink; ?>" class="single__share-link" target="_blank" rel="nofollow">
				<img src="<?php echo $template_directory_URI; ?>/dist/images/twitter.png" alt="<?php _e('Twitter', 'smd'); ?>" class="single__share-icon">
			</a>
		</li>
		<li class="single__share-item">
			<a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo $permalink; ?>&title=<?php echo $title; ?>" class="single__share-link" target="_blank" rel="nofollow">
				<img src="<?php echo $template_directory_URI; ?>/dist/images/linkedin.png" alt="<?php _e('LinkedIn', 'smd'); ?>" class="single__share-icon">
			</a>
		</li>
		<li class="single__share-item">
			<a href="https://plus.google.com/share?url=<?php echo $permalink; ?>" class="single__share-link" target="_blank" rel="nofollow">
				<img src="<?php echo $template_directory_URI; ?>/dist/images/google-plus.png" alt="<?php _e('Google+', 'smd'); ?>" class="single__share-icon">
			</a>
		</li>
	</ul>
</div>