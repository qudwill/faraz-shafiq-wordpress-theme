<?php
	$icon = get_field('git_icon', 'option');
	$title = get_field('git_title', 'option');
	$content = get_field('git_content', 'option');
	$phone_number = get_field('phone_number', 'option');
	$email = get_field('email', 'option');
?>
<div class="git">
	<div class="container">
		<?php if ( !empty($icon) ) : ?>
			<img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt']; ?>" class="git__icon">
		<?php endif; ?>
		<?php if ($title) : ?>
			<h3 class="git__title"><?php echo $title; ?></h3>
		<?php endif; ?>
		<?php if ($content) : ?>
			<div class="git__content"><?php echo $content; ?></div>
		<?php endif; ?>
		<div class="git__contact">
			<?php if ($phone_number) : ?>
				<div class="git__contact-button-wrapper">
					<a href="tel:<?php echo $phone_number; ?>" class="git__contact-button git__contact-button-phone vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-round vc_btn3-style-custom"><i class="fa fa-phone git__contact-button-fa"></i> <?php echo $phone_number; ?></a>
				</div>
			<?php endif; ?>
			<?php if ($email) : ?>
				<div class="git__contact-button-wrapper">
					<a href="mailto:<?php echo $email; ?>" class="git__contact-button git__contact-button-email vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-round vc_btn3-style-custom"><i class="fa fa-envelope git__contact-button-fa"></i> <?php echo $email; ?></a>
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>