<?php
	$title = get_field('title', 'option');
	$button_text = get_field('button_text', 'option');
	$button_link = get_field('button_link', 'option');
?>
<div class="cta" style="background-image: url('<?php the_field('background', 'option'); ?>');">
	<div class="container">
		<?php if ($title) : ?>
			<h3 class="cta__title"><?php echo $title; ?></h3>
		<?php endif; ?>
		<?php if ($button_text && $button_link) : ?>
			<div class="vc_btn3-container vc_btn3-center">
				<a href="<?php echo $button_link; ?>" class="cta__button vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-round vc_btn3-style-custom"><?php echo $button_text; ?></a>
			</div>
		<?php endif; ?>
	</div>
</div>