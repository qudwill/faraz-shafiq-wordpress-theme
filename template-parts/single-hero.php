<?php
	$minute_read = get_field('minute_read');

	global $has_post_thumbnail;
	global $title;
?>
<div class="hero" style="background: <?php the_field('color'); ?>;">
	<div class="container<?php if ($has_post_thumbnail) : ?> container--with-thumbnail<?php endif; ?>">
		<h1 class="hero__title"><?php echo $title; ?></h1>
		<div class="hero__meta group<?php if ($has_post_thumbnail) : ?> hero__meta--with-thumbnail<?php endif; ?>">
			<div class="hero__meta-author">
				<?php
					_e('Posted by', 'smd');

					echo ' ';

					the_author();
				?>
			</div>
			<?php if ($minute_read) : ?>
				<div class="hero__meta-time">
					<?php
						echo $minute_read . ' ';

						_e('Minute Read', 'smd');
					?>
				</div>
			<?php endif; ?>
			<div class="hero__meta-date">
				<?php
					_e('Date', 'smd');

					echo ': ';

					the_date('jS F Y');
				?>
			</div>
		</div>
	</div>
</div>