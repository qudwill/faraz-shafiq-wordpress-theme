<?php global $has_post_thumbnail; ?>
<div class="single-content">
	<div class="container">
		<?php if ($has_post_thumbnail) : ?>
			<div class="single-content__image">
				<?php the_post_thumbnail(null, 'full'); ?>
			</div>
		<?php endif; ?>
		<div class="single-content__body">
			<?php
				the_content();
				get_template_part('template-parts/single', 'share');
			?>
		</div>
	</div>
</div>