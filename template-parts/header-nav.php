<a href="javascript: void(0);" class="header__nav-button" id="header__nav-button">Menu</a>
<nav class="header__nav-inner<?php if ( is_front_page() ) : ?> header__nav-inner--home<?php endif; ?>" id="header__nav-inner">
	<?php
		wp_nav_menu(
			array(
			  'theme_location' => 'primary',
			  'container' => '',
			  'menu_class' => '',
			  'items_wrap' => '<ul class="header__nav-list group %2$s">%3$s</ul>',
			  'depth' => 1,
			)
		);
	?>
</nav>