<nav class="footer__bottom-nav">
	<?php
		wp_nav_menu(
			array(
			  'theme_location' => 'footer',
			  'container' => '',
			  'menu_class' => '',
			  'items_wrap' => '<ul class="footer__nav-list group %2$s">%3$s</ul>',
			  'depth' => 1,
			)
		);
	?>
</nav>