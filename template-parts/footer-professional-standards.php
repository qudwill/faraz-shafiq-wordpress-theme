<?php $professional_standards = get_field('icons', 'option'); ?>
<?php if ( !empty($professional_standards) ) : ?>
	<div class="footer__professional-standards">
		<h5 class="footer__widgets-title"><?php _e('Our Professional Standards', 'smd'); ?></h5>
		<ul class="footer__professional-standards-list group">
			<?php foreach ($professional_standards as $icon) : ?>
				<li class="footer__professional-standards-item">
					<img src="<?php echo $icon['icon']['url']; ?>" alt="<?php echo $icon['icon']['alt']; ?>">
				</li>
		<?php endforeach; ?>
		</ul>
	</div>
<?php endif; ?>