<?php
  class vcImagesSlider extends WPBakeryShortCode {
    function __construct() {
      add_action(
        'init',
        array(
          $this,
          'vc_images_slider_mapping'
        )
      );

      add_shortcode(
        'vc_images_slider',
        array(
          $this,
          'vc_images_slider_html'
        )
      );
    }
     
    public function vc_images_slider_mapping() {    
      if ( !defined('WPB_VC_VERSION') ) {
        return;
      }

      vc_map(
        array(
          'name' => __('VC Images Slider', 'smd'),
          'base' => 'vc_images_slider',
          'category' => __('Custom Content Elements', 'smd'),            
          'params' => array(
            array(
              'type' => 'attach_images',
              'heading' => __('Images', 'londontubemedia'),
              'param_name' => 'images',
              'admin_label' => false,
              'weight' => 0,
              'group' => 'Custom Group',
            ),
          ),
        )
      );         
    }

    public function vc_images_slider_html($atts) {
      extract(
        shortcode_atts(
          array(
            'images' => '',
          ),
          $atts
        )
      );

      $images_IDs_array = explode(',', $images);

      $html = '<div class="vc__images-slider" id="vc__images-slider">';

      foreach ($images_IDs_array as $image_ID) {
        $html .= wp_get_attachment_image($image_ID, 'full');
      }

      $html .= '</div>';
     
      return $html;
    }
  }
 
  new vcImagesSlider();    
?>