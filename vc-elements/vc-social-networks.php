<?php
  global $template_directory_uri;

  class vcSocialNetworks extends WPBakeryShortCode {
    function __construct() {
      add_action(
        'init',
        array(
          $this,
          'vc_social_networks_mapping'
        )
      );

      add_shortcode(
        'vc_social_networks',
        array(
          $this,
          'vc_social_networks_html'
        )
      );
    }
     
    public function vc_social_networks_mapping() {    
      if ( !defined('WPB_VC_VERSION') ) {
        return;
      }
         
      vc_map(
        array(
          'name' => __('VC Social Networks', 'smd'),
          'base' => 'vc_social_networks',
          'category' => __('Custom Content Elements', 'smd'),
        )
      );         
    }

    public function vc_social_networks_html() {
      extract( shortcode_atts( array(), null ) );

      $social_networks = get_field('social_networks', 'option');
      $html = '';

      if ( !empty($social_networks) ) {
        $html .= '<ul class="vc__social-networks-list">';

        foreach ($social_networks as $network) {
          $html .= '<li class="vc__social-networks-item">';
          $html .= '<a href="' . $network['link'] . '" class="vc__social-networks-link">';
          $html .= '<span class="vc__social-networks-helper"></span>';
          $html .= '<img src="' . $network['big_icon']['url'] . '" alt="' . $network['big_icon']['alt'] . '" class="vc__social-networks-icon">';
          $html .= '</a>'; 
          $html .= '</li>';
        }

        $html .= '</ul>';
      }
     
      return $html;
    }
  }
 
  new vcSocialNetworks();    
?>