<?php
  class vcProject extends WPBakeryShortCode {
    function __construct() {
      add_action(
        'init',
        array(
          $this,
          'vc_project_mapping'
        )
      );

      add_shortcode(
        'vc_project',
        array(
          $this,
          'vc_project_html'
        )
      );
    }
     
    public function vc_project_mapping() {    
      if ( !defined('WPB_VC_VERSION') ) {
        return;
      }
         
      vc_map(
        array(
          'name' => __('VC Project', 'smd'),
          'base' => 'vc_project',
          'category' => __('Custom Content Elements', 'smd'),            
          'params' => array(
            array(
              'type' => 'attach_image',
              'heading' => __('Background Image', 'smd'),
              'param_name' => 'background_image',
              'admin_label' => false,
              'weight' => 0,
              'group' => 'Project',
            ),
            array(
              'type' => 'textfield',
              'heading' => __('Title', 'smd'),
              'param_name' => 'title',
              'admin_label' => false,
              'weight' => 0,
              'group' => 'Project',
            ),
            array(
              'type' => 'textfield',
              'heading' => __('Subtitle', 'smd'),
              'param_name' => 'subtitle',
              'admin_label' => false,
              'weight' => 0,
              'group' => 'Project',
            ),
            array(
              'type' => 'vc_link',
              'heading' => __('Link', 'smd'),
              'param_name' => 'link',
              'admin_label' => false,
              'weight' => 0,
              'group' => 'Project',
            ),
            array(
              'type' => 'textfield',
              'heading' => __('Button Title', 'smd'),
              'param_name' => 'button_title',
              'admin_label' => false,
              'weight' => 0,
              'group' => 'Project',
            ),
            array(
              'type' => 'textfield',
              'heading' => __('Height', 'smd'),
              'param_name' => 'height',
              'admin_label' => false,
              'weight' => 0,
              'group' => 'Project',
            ),
          ),
        )
      );         
    }

    public function vc_project_html($atts) {
      extract(
        shortcode_atts(
          array(
            'background_image' => '',
            'title' => '',
            'subtitle' => '',
            'link' => '',
            'button_title' => '',
            'height' => '',
          ),
          $atts
        )
      );

      $background_image_URL = wp_get_attachment_image_url($background_image, 'full');
      $href = vc_build_link($link);
      $html = '<a href="' . $href['url'] . '" class="vc__project" style="background-image: url(' . $background_image_URL . '); height: ' . $height . ';">';

      $html .= '<div class="vc__project-gradient" style="height: ' . $height . ';">';
      $html .= '<div class="vc__project-info">';
      $html .= '<h3 class="vc__project-title">' . $title . '</h3>';
      $html .= '<div class="vc__project-subtitle">' . $subtitle . '</div>';
      $html .= '</div>';
      $html .= '<div class="vc__project-button">' . $button_title . '</div>';
      $html .= '</div>';
      $html .= '</a>';
     
      return $html;
    }
  }
 
  new vcProject();    
?>