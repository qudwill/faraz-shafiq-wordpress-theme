<?php
  class vcCaseMeta extends WPBakeryShortCode {
    function __construct() {
      add_action(
        'init',
        array(
          $this,
          'vc_case_meta_mapping'
        )
      );

      add_shortcode(
        'vc_case_meta',
        array(
          $this,
          'vc_case_meta_html'
        )
      );
    }
     
    public function vc_case_meta_mapping() {    
      if ( !defined('WPB_VC_VERSION') ) {
        return;
      }
         
      vc_map(
        array(
          'name' => __('VC Case Meta', 'smd'),
          'base' => 'vc_case_meta',
          'category' => __('Custom Content Elements', 'smd'),            
          'params' => array(
            array(
              'type' => 'textfield',
              'heading' => __('Client', 'smd'),
              'param_name' => 'client',
              'admin_label' => false,
              'weight' => 0,
              'group' => 'Case Meta',
            ),
            array(
              'type' => 'textfield',
              'heading' => __('Services', 'smd'),
              'param_name' => 'services',
              'admin_label' => false,
              'weight' => 0,
              'group' => 'Case Meta',
            ),
          ),
        )
      );         
    }

    public function vc_case_meta_html($atts) {
      extract(
        shortcode_atts(
          array(
            'client' => '',
            'services' => '',
          ),
          $atts
        )
      );

      $html = '<div class="vc__case-meta group">';

      if ($client != '') {
        $html .= '<div class="vc__case-meta-client"><span class="vc__case-meta-label">' . __('Client', 'smd') . ':</span> ' . $client . '</div>';
      }

      if ($services != '') {
        $html .= '<div class="vc__case-meta-services"><span class="vc__case-meta-label">' . __('Services', 'smd') . ':</span> ' . $services . '</div>';
      }

      $html .= '</div>';
     
      return $html;
    }
  }
 
  new vcCaseMeta();    
?>