<?php
  class vcLatestPosts extends WPBakeryShortCode {
    function __construct() {
      add_action(
        'init',
        array(
          $this,
          'vc_latest_posts_mapping'
        )
      );

      add_shortcode(
        'vc_latest_posts',
        array(
          $this,
          'vc_latest_posts_html'
        )
      );
    }
     
    public function vc_latest_posts_mapping() {    
      if ( !defined('WPB_VC_VERSION') ) {
        return;
      }
         
      vc_map(
        array(
          'name' => __('VC Latest Posts', 'smd'),
          'base' => 'vc_latest_posts',
          'category' => __('Custom Content Elements', 'smd'), 
        )
      );         
    }

    public function vc_latest_posts_html() {
      $args = array(
        'post_type' => 'post',
        'post_status' => 'publish',
        'posts_per_page' => 3,
      );

      $q = new WP_Query($args);
      $html = '';

      if ( $q->have_posts() ) {
        $html .= '<ul class="vc__latest-posts-list group">';

        while ( $q->have_posts() ) {
          $q->the_post();

          $post_thumbnail_URL = get_the_post_thumbnail_url(null, 'full');
          $innerClass = '';
          $innerStyle = '';

          if (!$post_thumbnail_URL) {
            $innerClass = ' vc__latest-posts-inner--no-bg';
            $innerStyle = ' style="background: ' . get_field('color') . ';"'; 
          }

          $html .= '<li class="vc__latest-posts-item">';
          $html .= '<a href="' . get_permalink() . '" class="vc__latest-posts-link" style="background-image: url(' . $post_thumbnail_URL . ');">';
          $html .= '<div class="vc__latest-posts-inner' . $innerClass . '"' . $innerStyle . '>';
          $html .= '<h3 class="vc__latest-posts-title">' . get_the_title() . '</h3>';
          $html .= '<div class="vc__latest-posts-meta">';

          $minute_read = get_field('minute_read');

          if ($minute_read) {
            $html .= '<div class="vc__latest-posts-minute">' . get_field('minute_read') . ' ' . __('minute read', 'smd') . '</div>';
          }

          $html .= '<div class="vc__latest-posts-date">' . __('Date', 'smd') . ': ' . get_the_date('jS F Y') . '</div>';
          $html .= '</div>';
          $html .= '</div>';
          $html .= '</a>';
          $html .= '</li>';
        }

        $html .= '</ul>';
      }

      return $html;
    }
  }
 
  new vcLatestPosts();    
?>