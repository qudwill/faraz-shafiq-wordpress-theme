<?php
  class vcQuote extends WPBakeryShortCode {
    function __construct() {
      add_action(
        'init',
        array(
          $this,
          'vc_quote_mapping'
        )
      );

      add_shortcode(
        'vc_quote',
        array(
          $this,
          'vc_quote_html'
        )
      );
    }
     
    public function vc_quote_mapping() {    
      if ( !defined('WPB_VC_VERSION') ) {
        return;
      }
         
      vc_map(
        array(
          'name' => __('VC Quote', 'smd'),
          'base' => 'vc_quote',
          'category' => __('Custom Content Elements', 'smd'),            
          'params' => array(
            array(
              'type' => 'textarea_html',
              'heading' => __('Content', 'smd'),
              'param_name' => 'content',
              'admin_label' => false,
              'weight' => 0,
              'group' => 'Quote',
            ),
          ),
        )
      );         
    }

    public function vc_quote_html($atts, $content) {
      extract( shortcode_atts(array(), $atts) );

      $content = wpb_js_remove_wpautop($content);

      return '<div class="vc__quote">' . $content . '</div>';
    }
  }
 
  new vcQuote();    
?>