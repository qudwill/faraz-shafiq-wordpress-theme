<?php
  class vcBlog extends WPBakeryShortCode {
    function __construct() {
      add_action(
        'init',
        array(
          $this,
          'vc_blog_mapping'
        )
      );

      add_shortcode(
        'vc_blog',
        array(
          $this,
          'vc_blog_html'
        )
      );
    }
     
    public function vc_blog_mapping() {    
      if ( !defined('WPB_VC_VERSION') ) {
        return;
      }
         
      vc_map(
        array(
          'name' => __('VC Blog', 'smd'),
          'base' => 'vc_blog',
          'category' => __('Custom Content Elements', 'smd'), 
        )
      );         
    }

    public function vc_blog_html() {
      $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

      $args = array(
        'post_type' => 'post',
        'post_status' => 'publish',
        'paged' => $paged,
      );

      $q = new WP_Query($args);
      $html = '';

      if ( $q->have_posts() ) {
        $html .= '<ul class="vc__latest-posts-list group">';

        while ( $q->have_posts() ) {
          $q->the_post();

          $post_thumbnail_URL = get_the_post_thumbnail_url(null, 'full');
          $innerClass = '';
          $innerStyle = '';

          if (!$post_thumbnail_URL) {
            $innerClass = ' vc__latest-posts-inner--no-bg';
            $innerStyle = ' style="background: ' . get_field('color') . ';"'; 
          }

          $html .= '<li class="vc__latest-posts-item">';
          $html .= '<a href="' . get_permalink() . '" class="vc__latest-posts-link" style="background-image: url(' . $post_thumbnail_URL . ');">';
          $html .= '<div class="vc__latest-posts-inner' . $innerClass . '"' . $innerStyle . '>';
          $html .= '<h3 class="vc__latest-posts-title">' . get_the_title() . '</h3>';
          $html .= '<div class="vc__latest-posts-meta">';

          $minute_read = get_field('minute_read');

          if ($minute_read) {
            $html .= '<div class="vc__latest-posts-minute">' . get_field('minute_read') . ' ' . __('minute read', 'smd') . '</div>';
          }

          $html .= '<div class="vc__latest-posts-date">' . __('Date', 'smd') . ': ' . get_the_date('jS F Y') . '</div>';
          $html .= '</div>';
          $html .= '</div>';
          $html .= '</a>';
          $html .= '</li>';
        }

        $html .= '</ul>';
      
        $html .= '<div class="vc__blog-pagination">';

        $html .= paginate_links(
          array(
            'base' => str_replace( 999999999, '%#%', esc_url( get_pagenum_link(999999999) ) ),
            'total' => $q->max_num_pages,
            'current' => max( 1, get_query_var('paged') ),
            'format' => '?paged=%#%',
            'show_all' => false,
            'type' => 'plain',
            'end_size' => 2,
            'mid_size' => 1,
            'prev_next' => false,
            'add_args' => false,
            'add_fragment' => '',
          )
        );

        $html .= '</div>';
      }

      return $html;
    }
  }
 
  new vcBlog();    
?>