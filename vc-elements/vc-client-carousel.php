<?php
  class vcClientCarousel extends WPBakeryShortCode {
    function __construct() {
      add_action(
        'init',
        array(
          $this,
          'vc_client_carousel_mapping'
        )
      );

      add_shortcode(
        'vc_client_carousel',
        array(
          $this,
          'vc_client_carousel_html'
        )
      );
    }
     
    public function vc_client_carousel_mapping() {    
      if ( !defined('WPB_VC_VERSION') ) {
        return;
      }

      vc_map(
        array(
          'name' => __('VC Client Carousel', 'smd'),
          'base' => 'vc_client_carousel',
          'category' => __('Custom Content Elements', 'smd'),            
          'params' => array(
            array(
              'type' => 'attach_images',
              'heading' => __('Images', 'londontubemedia'),
              'param_name' => 'images',
              'admin_label' => false,
              'weight' => 0,
              'group' => 'Custom Group',
            ),
          ),
        )
      );         
    }

    public function vc_client_carousel_html($atts) {
      extract(
        shortcode_atts(
          array(
            'images' => '',
          ),
          $atts
        )
      );

      $images_IDs_array = explode(',', $images);

      $html = '<div class="vc__client-carousel" id="vc__client-carousel">';

      foreach ($images_IDs_array as $image_ID) {
        $html .= '<div class="vc__client-carousel-item">';
        $html .= '<div class="vc__client-carousel-helper"></div>';
        $html .= wp_get_attachment_image($image_ID, 'full');
        $html .= '</div>';
      }

      $html .= '</div>';
     
      return $html;
    }
  }
 
  new vcClientCarousel();    
?>