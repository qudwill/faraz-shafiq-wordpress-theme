<?php
  class vcPlanet extends WPBakeryShortCode {
    function __construct() {
      add_action(
        'init',
        array(
          $this,
          'vc_planet_mapping'
        )
      );

      add_shortcode(
        'vc_planet',
        array(
          $this,
          'vc_planet_html'
        )
      );
    }
     
    public function vc_planet_mapping() {    
      if ( !defined('WPB_VC_VERSION') ) {
        return;
      }
         
      vc_map(
        array(
          'name' => __('VC Planet', 'smd'),
          'base' => 'vc_planet',
          'category' => __('Custom Background Elements', 'smd'),            
          'params' => array(
            array(
              'type' => 'attach_image',
              'heading' => __('Image', 'smd'),
              'param_name' => 'image',
              'admin_label' => false,
              'weight' => 0,
              'group' => 'Planet',
            ),
            array(
              'type' => 'dropdown',
              'heading' => __('Position', 'smd'),
              'param_name' => 'position',
              'admin_label' => false,
              'weight' => 0,
              'group' => 'Planet',
              'value' => array(
                'Left' => 'left',
                'Left (Out)' => 'left_out',
                'Right' => 'right',
                'Right (Out)' => 'right_out',
                'Center' => 'center',
              ),
            ),
          ),
        )
      );         
    }

    public function vc_planet_html($atts) {
      extract(
        shortcode_atts(
          array(
            'image' => '',
            'position' => 'left',
          ),
          $atts
        )
      );

      $img_meta = wp_get_attachment_metadata($image);
      $img_URL = wp_get_attachment_image_url($image, 'full');
      $class_name = 'vc__planet vc__planet--' . $position;
      $bg_position = 'center';
      $data_tags = '';

      if ($position === 'right' || $position === 'right_out') {
        $bg_position .= ' 65%';
        $data_tags .= 'data-0="background-position: center 65%;" data-630="background-position: center 10%;"';
      } elseif ($position === 'left' || $position ==='left_out') {
        $bg_position = ' 40%';
        $data_tags .= 'data-0="background-position: center 40%;" data-630="background-position: center 80%;"';
      } elseif ($position === 'center') {
        $bg_position = ' 55%';
        $data_tags .= 'data-0="background-position: center 55%;" data-630="background-position: center 10%;"';
      }

      $outer_styles = '';
      $outer_position = $img_meta['width'] + 40;

      if ($position === 'left_out') {
        $outer_styles = ' left: -' . $outer_position / 2 . 'px;';
      } elseif ($position === 'right_out') {
        $outer_styles = ' right: -' . $outer_position / 2 . 'px;';
      }

      $html = '<div class="' . $class_name . ' skrollable" style="background-position: ' . $bg_position . '; width: ' . $img_meta['width'] . 'px; background-image: url(' . $img_URL . ');' . $outer_styles . '" ' . $data_tags . '></div>';
     
      return $html;
    }
  }
 
  new vcPlanet();    
?>