<?php
  class vcGoogleMap extends WPBakeryShortCode {
    function __construct() {
      add_action(
        'init',
        array(
          $this,
          'vc_google_map_mapping'
        )
      );

      add_shortcode(
        'vc_google_map',
        array(
          $this,
          'vc_google_map_html'
        )
      );
    }
     
    public function vc_google_map_mapping() {    
      if ( !defined('WPB_VC_VERSION') ) {
        return;
      }
         
      vc_map(
        array(
          'name' => __('VC Google Map', 'smd'),
          'base' => 'vc_google_map',
          'category' => __('Custom Content Elements', 'smd'),            
          'params' => array(
            array(
              'type' => 'textfield',
              'heading' => __('Latitude', 'smd'),
              'param_name' => 'lat',
              'admin_label' => false,
              'weight' => 0,
              'group' => 'Coordinates',
            ),
            array(
              'type' => 'textfield',
              'heading' => __('Longitude', 'smd'),
              'param_name' => 'lng',
              'admin_label' => false,
              'weight' => 0,
              'group' => 'Coordinates',
            ),
          ),
        )
      );         
    }

    public function vc_google_map_html($atts) {
      extract(
        shortcode_atts(
          array(
            'lat' => '',
            'lng' => '',
          ),
          $atts
        )
      );

      wp_enqueue_script('google-maps-js-api');

      $html = '<div class="vc__google-map" id="vc__google-map"></div>';

      $html .= '
        <script>
          function initMap() {
            var center = {
              lat: ' . $lat . ',
              lng: ' . $lng . '
            };

            var map = new google.maps.Map(document.getElementById(\'vc__google-map\'), {
              center: center,
              zoom: 15
            });

            var marker = new google.maps.Marker({
              position: center,
              map: map
            });
          }
        </script>
      ';
     
      return $html;
    }
  }
 
  new vcGoogleMap();    
?>