<?php
	/**
	 ** Template Name: Homepage
	 */

	get_header();
	get_template_part('template-parts/background', 'slider');
	get_template_part('template-parts/slider', 'overlay');
	get_template_part('template-parts/slider', 'title');

	if ( have_posts() ) {
		the_post();
		get_template_part('template-parts/info', 'box');
	}

	get_footer();
?>