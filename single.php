<?php
	get_header();
	
	if ( have_posts() ) {
		the_post();

		$has_post_thumbnail = false;
	
		if ( has_post_thumbnail() ) {
			$has_post_thumbnail = true;
		}

		$title = get_the_title();

		get_template_part('template-parts/single', 'hero');
		get_template_part('template-parts/single', 'content');
		get_template_part('template-parts/single', 'latest-posts');
	}

	get_footer();
?>